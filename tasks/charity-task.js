task("balance", "Display contract balance")
    .addParam("contract", "The address of contract")
    .setAction(async (taskArgs) => {
        const Charity = await ethers.getContractFactory("Charity");
        const contract = await Charity.attach(taskArgs.contract);

        console.log('Charity contract address: ', contract.address);
        console.log('Contract balance: ', ethers.utils.formatEther(await contract.balance()));
    });

task("donate", "Donate eth to contract")
    .addParam("contract", "The address of contract")
    .addParam("amount", "Amount of eth for donate")
    .setAction(async (taskArgs) => {
        const Charity = await ethers.getContractFactory("Charity");
        const contract = await Charity.attach(taskArgs.contract);

        await contract.makeDonation({ value: ethers.utils.parseEther(taskArgs.amount) });
        console.log(`Successfully donate: ${taskArgs.amount} eth`);
    });

task("withdraw", "Withdraw eth from contract to address")
    .addParam("contract", "The address of contract")
    .addParam("amount", "Amount of eth for donate")
    .addParam("address", "Address where withdraw coins")
    .setAction(async (taskArgs) => {
        const Charity = await ethers.getContractFactory("Charity");
        const contract = await Charity.attach(taskArgs.contract);

        await contract.withdrawToAddress(taskArgs.address, ethers.utils.parseEther(taskArgs.amount));

        console.log(`Successfully withdraw: ${taskArgs.amount} eth to ${taskArgs.address}`);
    });

task("benefactors", "Show addresses of all benefactors")
    .addParam("contract", "The address of contract")
    .setAction(async (taskArgs) => {
        const Charity = await ethers.getContractFactory("Charity");
        const contract = await Charity.attach(taskArgs.contract);

        const benefactorAddresses = await contract.getBenefactorAddresses();


        for (const address of benefactorAddresses) {
            console.log(`Address: ${address}`);
        }
    });

task("benefactor_sum", "Show addresses of all benefactors")
    .addParam("contract", "The address of contract")
    .addParam("address", "The address of benefactor")
    .setAction(async (taskArgs) => {
        const Charity = await ethers.getContractFactory("Charity");
        const contract = await Charity.attach(taskArgs.contract);

        const benefactorTotalDonation = await contract.benefactorDonationSum(taskArgs.address);


        console.log(`Befactor ${taskArgs.address}. Total donation: ${ethers.utils.formatEther(benefactorTotalDonation)} eth`)
    });