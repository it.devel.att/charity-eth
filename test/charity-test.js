const { expect } = require("chai");
const { ethers } = require("hardhat");


describe("Charity", function () {
  let provider;

  let contract;
  let owner;
  let addr1;
  let addr2;
  let addrs;

  beforeEach(async () => {
    provider = ethers.provider;
    const Charity = await ethers.getContractFactory("Charity");
    [owner, addr1, addr2, ...addrs] = await ethers.getSigners();
    contract = await Charity.deploy();
  });


  it("Should deploy with owner", async function () {
    expect(await contract.owner()).to.equal(owner.address);
  });

  it("Should make donation and increase contract balance", async function () {
    await contract.connect(addr1).makeDonation({ value: ethers.utils.parseEther("1") });

    expect(await contract.balance()).to.equal(ethers.utils.parseEther("1"));
  });

  it("Only owner can withdraw contract amount to address", async function () {
    await contract.connect(addr1).makeDonation({ value: ethers.utils.parseEther("10") });

    await expect(contract.connect(addr2).withdrawToAddress(addr2.address, ethers.utils.parseEther("7"))).to.be.revertedWith('Ownable: caller is not the owner');
  });

  it("Owner should withdraw contract amount to address", async function () {
    await contract.connect(addr1).makeDonation({ value: ethers.utils.parseEther("10") });

    const addr2BalanceBefore = await provider.getBalance(addr2.address);

    await contract.withdrawToAddress(addr2.address, ethers.utils.parseEther("7"));

    const addr2BalanceAfter = await provider.getBalance(addr2.address);



    expect(await contract.balance()).to.equal(ethers.utils.parseEther("3"));
    expect(addr2BalanceAfter).to.be.equal(addr2BalanceBefore.add(ethers.utils.parseEther("7")));
  });

  it("Should return sum of donations for address", async function () {
    await contract.connect(addr1).makeDonation({ value: ethers.utils.parseEther("1") });
    await contract.connect(addr1).makeDonation({ value: ethers.utils.parseEther("2") });
    await contract.connect(addr1).makeDonation({ value: ethers.utils.parseEther("3") });

    const donations = await contract.benefactorDonationSum(addr1.address);
    expect(donations).to.eq(ethers.utils.parseEther("6"));
  });


  it("Should make donation and write to logs and not duplicates", async function () {
    await contract.connect(addr1).makeDonation({ value: ethers.utils.parseEther("1") });
    await contract.connect(addr1).makeDonation({ value: ethers.utils.parseEther("2") });
    await contract.connect(addr1).makeDonation({ value: ethers.utils.parseEther("3") });

    const benefactorAddresses = await contract.getBenefactorAddresses();

    expect(benefactorAddresses.length).to.eq(1);
    expect(benefactorAddresses[0]).to.equal(addr1.address);
  });
});
