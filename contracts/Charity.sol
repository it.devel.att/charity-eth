//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract Charity is Ownable {

    mapping(address => uint256[]) private benefactors;
    address[] private benefactorAddresses;

    mapping(address => uint256[]) private withdraws;

    function makeDonation() public payable {
        if (benefactors[msg.sender].length == 0) {
            benefactorAddresses.push(msg.sender);
        }
        benefactors[msg.sender].push(msg.value);
    }

    function getBenefactorAddresses() public view returns(address []memory) {
        return benefactorAddresses;
    }

    function benefactorDonationSum(address _benefactor) public view returns(uint256) {
        uint256 sum;
        for (uint i = 0; i < benefactors[_benefactor].length; i++ ) {
            sum += benefactors[_benefactor][i];
        }
        return sum;
    }

    // Only owner function

    function balance() public view onlyOwner returns(uint256) {
        return _balance();
    }

    function withdrawToAddress(address _to, uint _amount) public onlyOwner {
        address payable wallet = payable(_to);
        wallet.transfer(_amount);
        withdraws[_to].push(_amount);
    }

    // Private functions

    function _balance() private view returns(uint256) {
        return address(this).balance;
    }
}
