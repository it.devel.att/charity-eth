# Basic Sample Hardhat Project

This project demonstrates a basic Hardhat use case. It comes with a sample contract, a test for that contract, a sample script that deploys that contract, and an example of a task implementation, which simply lists the available accounts.

Try running some of the following tasks:

```shell
npx hardhat accounts
npx hardhat compile
npx hardhat clean
npx hardhat test
npx hardhat node
node scripts/sample-script.js
npx hardhat help
```

For check test coverage
```
npx hardhat coverage
```

Create .env file for store secrets:

File must contains
```
RINKEBY_PRIVATE_KEY=*YOUR_PRIVATE_RINKBEY_KEY*
LOCAL_PRIVATE_KEY=*YOUR_LOCAL_PRIVATE_KEY*
```

Need to export file into source (in every new opened terminal)
```
export $(cat .env)
```

Local deploy:

Run node in separate window
```sh
npx hardhat node
```
Output
```
Accounts
========

WARNING: These accounts, and their private keys, are publicly known.
Any funds sent to them on Mainnet or any other live network WILL BE LOST.

Account #0: 0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266 (10000 ETH)
Private Key: 0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80
....
```
> This private key **0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80** need to put in .env into **LOCAL_PRIVATE_KEY** value

Deploy
```
npx hardhat run scripts/deploy.js --network local
```
Output:
```
Charity deployed to: 0xe7f1725E7734CE288F8367e1Bb143E90bb3F0512
```
> This command deploy contract to local netwrok and return address of this contract

Contract tasks:

Check contract balance:
```console
yaroslav@yaroslav:~/eth-charity$ npx hardhat balance --contract 0x5FbDB2315678afecb367f032d93F642f64180aa3 --network local
Charity contract address:  0x5FbDB2315678afecb367f032d93F642f64180aa3
Contract balance:  0.0
```

Donate to contract
```console
yaroslav@yaroslav:~/eth-charity$ npx hardhat donate --contract 0x5FbDB2315678afecb367f032d93F642f64180aa3 --amount 1 --network local
Successfully donate: 1 eth
```

For donate from different accounts
```console
yaroslav@yaroslav:~/eth-charity$ LOCAL_PRIVATE_KEY=0x701b615bbdfb9de65240bc28bd21bbc0d996645a3dd57e7b12bc2bdf6f192c82 npx hardhat donate --contract 0x5FbDB2315678afecb367f032d93F642f64180aa3 --network local --amount 13
Successfully donate: 13 eth
```
> Set LOCAL_PRIVATE_KEY from one of the node console you launch

Withdraw from contract to address
```console
yaroslav@yaroslav:~/eth-charity$ npx hardhat withdraw --contract 0x5FbDB2315678afecb367f032d93F642f64180aa3 --address 0x3c44cdddb6a900fa2b585dd299e03d12fa4293bc --amount 20 --network local
Successfully withdraw: 20 eth to 0x3c44cdddb6a900fa2b585dd299e03d12fa4293bc
```

Benefactors addresses:
```console
yaroslav@yaroslav:~/eth-charity$ npx hardhat benefactors --contract 0x5FbDB2315678afecb367f032d93F642f64180aa3 --network local
Address: 0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266
Address: 0x3C44CdDdB6a900fa2b585dd299e03d12FA4293BC
Address: 0x15d34AAf54267DB7D7c367839AAf71A00a2C6A65
```

Donator total amount of donation
```console
yaroslav@yaroslav:~/eth-charity$ npx hardhat benefactor_sum --contract 0x0165878A594ca255338adfa4d48449f69242Eb8F --network local --address 0x15d34aaf54267db7d7c367839aaf71a00a2c6a65
Befactor 0x15d34aaf54267db7d7c367839aaf71a00a2c6a65. Total donation: 30.0 eth
```

> Note that **--contract** may be changed, this just for examples


```sh
npx hardhat coverage
```
Test coverage
```console
--------------|----------|----------|----------|----------|----------------|
File          |  % Stmts | % Branch |  % Funcs |  % Lines |Uncovered Lines |
--------------|----------|----------|----------|----------|----------------|
 contracts/   |      100 |      100 |      100 |      100 |                |
  Charity.sol |      100 |      100 |      100 |      100 |                |
--------------|----------|----------|----------|----------|----------------|
All files     |      100 |      100 |      100 |      100 |                |
--------------|----------|----------|----------|----------|----------------|
```