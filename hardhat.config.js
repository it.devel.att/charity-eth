require("@nomiclabs/hardhat-waffle");
require('solidity-coverage');
require('./tasks/charity-task');


task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

const privateKey = process.env.RINKEBY_PRIVATE_KEY || "NOT_SET";
const localPrivateKey = process.env.LOCAL_PRIVATE_KEY || "NET_SET";

module.exports = {
  solidity: "0.8.4",
  defaultNetwork: "hardhat",
  networks: {
    hardhat: {
    },
    local: {
      url: "http://127.0.0.1:8545/",
      accounts: [localPrivateKey]
    },
    rinkeby: {
      url: "https://rinkeby.infura.io/v3/f1812db3eb924af6980151b8708271a7",
      accounts: [privateKey]
    }
  },
  paths: {
    sources: "./contracts",
    tests: "./test",
    cache: "./cache",
    artifacts: "./artifacts"
  },
  mocha: {
    timeout: 40000
  }
};
